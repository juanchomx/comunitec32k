-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `comunitec32k_tst`;
CREATE TABLE `comunitec32k_tst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunitec_tbl_bitacora`;
CREATE TABLE `comunitec_tbl_bitacora` (
  `id_entrada` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` smallint(6) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_entrada`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunitec_tbl_motivos_de_visita`;
CREATE TABLE `comunitec_tbl_motivos_de_visita` (
  `id_motivos_de_visita` tinyint(4) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `campo_valido` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_motivos_de_visita`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunitec_tbl_registro_visitas`;
CREATE TABLE `comunitec_tbl_registro_visitas` (
  `no_de_visita` smallint(6) NOT NULL AUTO_INCREMENT,
  `id_usuario` smallint(6) NOT NULL,
  `fecha_visita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_motivos_de_visita` tinyint(4) NOT NULL,
  PRIMARY KEY (`no_de_visita`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunitec_tbl_tipos_de_usuario`;
CREATE TABLE `comunitec_tbl_tipos_de_usuario` (
  `id_tipo_usuario` tinyint(4) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipo_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunitec_tbl_usuarios`;
CREATE TABLE `comunitec_tbl_usuarios` (
  `id_usuario` smallint(6) NOT NULL AUTO_INCREMENT,
  `ap_Paterno` varchar(20) NOT NULL,
  `ap_Materno` varchar(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `f_nacimiento` date NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `organizacion` varchar(20) NOT NULL,
  `colonia` varchar(255) DEFAULT NULL,
  `id_tipo_usuario` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `comunitec_tst_tbl_usuarios`;
CREATE TABLE `comunitec_tst_tbl_usuarios` (
  `id_usuario` smallint(6) NOT NULL AUTO_INCREMENT,
  `ap_Paterno` varchar(20) NOT NULL,
  `ap_Materno` varchar(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `f_nacimiento` date NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `organizacion` varchar(20) NOT NULL,
  `colonia` varchar(255) DEFAULT NULL,
  `id_tipo_usuario` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- 2020-05-15 15:00:00
