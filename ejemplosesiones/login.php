<?
	require_once('connection.php');
	session_start();
	///Este es el modelo

	if( $_SESSION['error'] !== '' ){
		$error = $_SESSION['error'];
		///unset($_SESSION['error']);
	}
	session_destroy();
	
	if( isset($_POST['acceder']) ){
		$login = 1;
		
		if( (isset($_POST['usuario'])) && ((trim($_POST['usuario'])) !== '')){
			if( (isset($_POST['pwd'])) && ((trim($_POST['pwd'])) !== '')){				
				$usr = trim($_POST['usuario']);
				$pwd = trim(($_POST['pwd']));
				
				
				$qry = "SELECT * FROM `usuarios` 
							WHERE nombre_usuario = :usuario AND contrasena = :contrasena";

				try{
					$stmt = $link->prepare($qry);
					$stmt->execute(array(
							':usuario' => $usr,
							':contrasena' => $pwd)
						);
					$row = $stmt->fetch(PDO::FETCH_ASSOC);
					
					if( $row['id_usuario']>0 ){
						session_start();
						$success = 'Usuario encontrado';
						$_SESSION['login'] = 1;
						
						$_SESSION['id_usuario'] = htmlentities($row['id_usuario']);
						$_SESSION['nombre'] = htmlentities($row['nombre_usuario']);
						$_SESSION['telefono'] = htmlentities($row['telefono']);
						$_SESSION['correo_electronico'] = htmlentities($row['correo_electronico']);
				
						header("Location: index.php");
						return;						
					}
					else
						$error = 'Credenciales no validas o usuario no existe';
					
					
				}catch(Exception $ex){
					echo '<img src="img/oops.png" alt="Oops" width="100" height="100">';
					echo '<h3>Hubo un error, favor de contactar a soporte</h3>';
					$_SESSION['error'] = 'Hubo un error, favor de contactar a soporte';				
					error_log("login.php: acceder(), SQL error= ".$ex->getMessage());
					return;
				}
				
			}
			else{
				$error = 'Campo contraseña no debe estar vacío';
			}			
		}
		else{
			$error = 'Campo usuario no debe estar vacío';
		}
	}
	
	///A partir de aqui abajo inicamos la vista
?>
<html>
<body>
	<h1>Hola. Favor de acceder a través de sus credenciales</h1>
	<?		
		if( isset($error) ){
			echo '<h3 style="color: red;">'.$error.'</h3>';
			unset($error);
		
		}else{
			if( isset($success) ){
				echo '<h3 style="color: green;">'.$success.'</h3>';
				unset($error);				
			}
		}		
	?>
	<div>
		<form method="post">
			<label>Usuario</label>
			<input type="text" name="usuario" placeholder="Luke Skywalker" required>
			<br><br>
			<label>Contraseña</label>
			<input type="password" name="pwd" >
			<br><br>
			<input type="submit" name="acceder" value="Acceder">
		</form>
	</div>
</body>
</html>