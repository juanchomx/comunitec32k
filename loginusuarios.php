<?php include("login.php"); ?>

<!DOCTYPE html>
<html lang="en">
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initialscale=1">
 <title>Comunitec32k</title>

 <!-- Bootstrap -->
 <link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/mystyle.css">
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5
elements and media queries -->
 <!-- WARNING: Respond.js doesn't work if you view the
page via file:// -->
 <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/libs/html5shiv/
3.7.0/html5shiv.js"></script>
 <script src="https://oss.maxcdn.com/libs/respond.js/
1.4.2/respond.min.js"></script>
 <![endif]-->

 </head>

<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="main.php">Comunitec32k</a>
		</div>
		<ul class="nav navbar-nav">
		  <li><a href="registrarse.php">Registrarse</a></li>
		  <li class="active"><a href="#">Login</a></li>
		  <li><a href="">Quienes Somos</a></li>
		  <li><a href="">Acerca de</a></li>
		</ul>
	  </div>
	</nav>

    <div class="container contentContainer" id="topContainer" >
        <div class="row">
            <div class="col-md-6 col-md-offset-3" id="topRow" >
							
							<?php
								if($error){
									echo '<div class="alert alert-danger">'.addslashes($error).'</div>';
								}
								if($message){
									echo '<div class="alert alert-success">'.addslashes($message).'</div>';
								}
							?>				

				<img src="img/logo.png" alt="Comunitec32k" height="125" width="125" >
				</br>                
				<h1 class="marginTop">Comunidad Tecnologica del centro</h1>
				<p class="mainMsg" >Bienvenidos al mejor espacio para acceder a la tecnologia.</p>
				<p>Escribe tu correo para registrar tu visita</p>
							
                <form class="navbar-form navbar-center" method="post">
					<div class="form-group">
                        <input type="email" name="loginEmail" placeholder="Email" class="form-control"  />
                    </div>
					<!--
                    <div class="form-group">
                        <input type="password" name=" loginPassword" placeholder="Password" class="form-control" />
                    </div>
					
					<br>
					-->
					<br>
                     <input type="submit" name="login" class="btn btn-success btn-lg marginTop" value="Registra Tu Visita" />
                </form>  
							
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/
jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files
    as needed -->
    <script src="js/bootstrap.min.js"></script>
        
    <script>
        //$("#topContainer").css("height", $(window).height());
        $(".contentContainer").css("min-height", $(window).height());

		$(".dropdown-menu li a").click(function(){
		  var selText = $(this).text();
		  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
		});		
		
    </script>

</body>
</html> 

