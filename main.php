<?php include("login.php"); ?>
<!DOCTYPE html>
<html lang="en">
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initialscale=1">
 <title>Comunitec32k</title>

 <!-- Bootstrap -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	 
	 
<link rel="stylesheet" type="text/css" href="css/mystyle.css">
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5
elements and media queries -->
 <!-- WARNING: Respond.js doesn't work if you view the
page via file:// -->
 <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/libs/html5shiv/
3.7.0/html5shiv.js"></script>
 <script src="https://oss.maxcdn.com/libs/respond.js/
1.4.2/respond.min.js"></script>
 <![endif]-->

 </head>

<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="main.php">Comunitec32k </a> 
		  
		</div>
		<ul class="nav navbar-nav">
		</ul>
		<ul class="nav navbar-nav navbar-right">
		</ul>
	  </div>
	</nav>

    </div>

    <div class="container contentContainer" id="topContainer" >
        <div class="row">
            <div class="col-md-6 col-md-offset-3" id="topRow" >
				<img src="img/logo.png" alt="Comunitec32k" >
				</br>                
				<h3 class="marginTop">Comunidad Tecnol&#243gica del centro</h3>
				<p class="mainMsg" >Sitio de prueba. Bienvenidos al mejor espacio para acceder a la tecnolog&#237a.</p>
				<br>
				<h3><span><img src="img/chrome.png" height="50" width="50">Por el momento el desempeño óptimo del portal se da en el navegador Google Chrome</span></h3>	<br>
				<?php
					if($error){
						echo '<div class="alert alert-danger">'.addslashes($error).'</div>';
						
					}
					
					if($message){
						echo '<div class="alert alert-success">'.addslashes($message).'</div>';
						
					}
				
				?>
    </div>


    <div class="container contentContainer" id="topContainer" >
        <div class="row">
            <div class="col-md-6 col-md-offset-3" id="topRow" >

							<h2>¿Ya est&#225s registrado(a)? Favor de apuntar tu visita de hoy</h2>
							<form class="navbar-form navbar-center" method="post">
								<br>
							 <?								
								echo '<label>Motivo de la Visita</label>&nbsp;';
								$query = "SELECT * FROM comunitec_tbl_motivos_de_visita order by descripcion asc";
								$result = mysqli_query($link, $query);
								//showUser(this.value)
								echo '<select class="form-control" id=",motivovisita" name="motivovisita" onchange="">';
									$rowCount=1;
									  while($row = $result->fetch_assoc()){   //Creates a loop to loop through results
											$id_motivos_de_visita = $row['id_motivos_de_visita'];
											$descripcion = $row['descripcion'];
											echo '<option value='.$id_motivos_de_visita.'>'.$descripcion.'</option>';
									  }
								  echo '</select><br><br>';
								?>				
								<!--
								<div class="form-group">
									<label for="motivovisita">Motivo de la Visita</label><br>
									<select class="form-control" id="motivovisita" name="motivovisita" value="Motivo de la Visita">
										<option value="1">Visita General</option> <!-- first option contains value="" 
										<option value="2">Informaci&#243;n</option> 
										<option value="3">Rob&#243;tica (Mexh Robotix)</option>
										<option value="4">Electr&#243;nica (Cenaltec)</option>
										<option value="5">Curso Solid Works</option>
										<option value="6">Taller Programaci&#243;n Web (ICATECH)</option>
										<option value="7">Taller Manejo Marketplace (ICATECH)</option>
										<option value="8">Impresi&#243;n 3D</option>
										<option value="9">Computa&#243;n</option>
									</select>
								</div>
								-->
								<br>
								<br>
								<div class="form-group">
									<!--
									<input type="email" name="loginEmail" placeholder="Tel&#233fono o Email" class="form-control"  />
									-->
									<input type="text" name="loginEmail" placeholder="Tel&#233fono o Email" class="form-control"  />
									
								</div>

								<br>
								 <input type="submit" name="login" class="btn btn-success btn-lg marginTop" value="Registra Tu Visita" />
							</form>  					
							<hr>
							<h2>¿Primera vez que nos visitas? Favor de registrarse:</h2>
							<form class="marginTop" method="post" action="#" onsubmit="if(document.getElementById('agree').checked) { return true; } else { alert('Favor de leer las politicas de privacidad de la comunidad tecnologica del centro.'); return false; }">
										<div class="form-group">
												<label for="ap_paterno">Apellido Paterno</label>
												<input type="text" name="ap_paterno" class="form-control" >
										</div>										
										
										<div class="form-group">
												<label for="ap_materno">Apellido Materno</label>
												<input type="text" name="ap_materno" class="form-control" >
										</div>										
										
										<div class="form-group">
												<label for="nombre">Nombre</label>
												<input type="text" name="nombre" class="form-control" >
										</div>	

										<div class="form-group">
												<label for="f_nacim">Fecha de Nacimiento (<mark>Mes-Dia_A&#241o</mark>)</label>
												<input type="date" name="f_nacim" class="form-control" placeholder="MM-DD-YYYY">
										</div>											
							
										<br>
										<br>
										<div class="form-group">
												<label for="tel">Tel&#233fono  </label>
												<input type="tel" name="tel" placeholder="0123456789" pattern="^\d{10}$" required >
										</div>										
										<br>										
										<div class="form-group">
												<label for="organizacion">Empresa/Institucion/Escuela</label>
												<input type="text" name="organizacion" class="form-control" >
										</div>										
										<br>
										<div class="form-group">
												<label for="colonia">Colonia (opcional)</label>
												<input type="text" name="colonia" class="form-control" >
										</div>										
										<br>										
										<div class="form-group">
												<label for="email">Correo Electrónico</label>
												<input type="email" name="email" class="form-control" placeholder="alguien@mail.com" value="<?php addslashes($email) ?>"
										</div>									

							<input type="checkbox" name="checkbox" value="check" id="agree" /> He leido la <a href="politicaprivacidad.php">politica de privacidad</a> de la comunidad tecnologica del centro. <br>
							
							<input type="submit" name="submit" class="btn btn-success btn-lg marginTop" value="Registrar Nuevo Usuario(a)" />
						</form>
            </div>
        </div>
    </div>
	
	
		<footer>
			<p>Posted by: Syner</p>
			<p>Contact information: <a href="mailto:jmunoz@syner.info">
			jmunoz@syner.info</a>.</p>
		</footer>	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/
jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files
    as needed -->
    <script src="js/bootstrap.min.js"></script>
        
    <script>
        //$("#topContainer").css("height", $(window).height());
        $(".contentContainer").css("min-height", $(window).height());

		$(".dropdown-menu li a").click(function(){
		  var selText = $(this).text();
		  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
		});		
		
    </script>

</body>
</html> 

